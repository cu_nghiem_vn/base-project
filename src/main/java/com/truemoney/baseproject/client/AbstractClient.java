package com.truemoney.baseproject.client;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import com.truemoney.baseproject.client.exception.AccessThirdPartyException;
import com.truemoney.baseproject.client.exception.InteractiveThirdPartyException;
import com.truemoney.baseproject.util.MaskingSensitiveDataUtil;
import com.truemoney.baseproject.util.Util;

public abstract class AbstractClient {

  private static Logger logger = LoggerFactory.getLogger(AbstractClient.class);

  public abstract RestTemplate getRestTemplate();

  public abstract String getBaseUri();
  
  public abstract String getClientName();

  public <T> T exchange(String uri, HttpMethod method, MediaType mediaType, Map<String, String> headers, Map<String, String> uriVariables, Object request, Class<T> clazz) throws AccessThirdPartyException {
    T response = null;
    try {
      String url = getBaseUri() + uri;
      HttpHeaders reqheaders = new HttpHeaders();
      reqheaders.setContentType(mediaType);
      if (headers != null) {
        for (Map.Entry<String, String> entry : headers.entrySet()) {
          reqheaders.add(entry.getKey(), entry.getValue());
        }
      }
      @SuppressWarnings("rawtypes")
      HttpEntity entity = null;
      if (request == null) {
        logger.info("3rd REQ [{}, {}]", getClientName(), url);
        entity = new HttpEntity<>(reqheaders);
      } else {
        logger.info("3rd REQ [{}, {}, {}]", getClientName(), url, getStrFromObject(mediaType, request));
        entity = new HttpEntity<>(request, reqheaders);
      }

      ResponseEntity<T> responseEntity = null;
      if (uriVariables == null || uriVariables.isEmpty()) {
        responseEntity = getRestTemplate().exchange(url, method, entity, clazz);
      } else {
        responseEntity = getRestTemplate().exchange(url, method, entity, clazz, uriVariables);
      }

      response = responseEntity.getBody();
      if (response != null)
        logger.info("3rd RESP [{}, {}]", getClientName(), getStrFromObject(responseEntity.getHeaders().getContentType(), response));

    } catch (HttpClientErrorException | HttpServerErrorException e) {
      int httpStatusCode = e.getRawStatusCode();
      String message = e.getResponseBodyAsString();
      logger.error("Fail when communicating to 3rd [{}, {}, {}]", getClientName(), httpStatusCode, message);
      throw new InteractiveThirdPartyException(httpStatusCode, message);
    }catch (ResourceAccessException e) {
      String message = e.getMessage();
      logger.error("Fail when connecting to 3rd [{}, {}]", getClientName(), message);
      throw new AccessThirdPartyException(message);
    }
    return response;
  }

  private String getStrFromObject(MediaType mediaType, Object data) {
    if(mediaType == MediaType.APPLICATION_JSON) {
      return MaskingSensitiveDataUtil.maskSensitiveInfo(Util.objectToJson(data), false);
    }
    return data.toString();
  }

}
