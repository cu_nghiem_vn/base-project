package com.truemoney.baseproject.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import com.truemoney.baseproject.client.exception.AccessThirdPartyException;

@Component
public class ExampleClient extends AbstractClient{
  public static final String CLIENT_NAME = "example";
  @Autowired
  private RestTemplate restTemplate;
  
  @Value("${application.base-project.3rd.example.base-url}")
  private String baseUri;
  
  @Override
  public RestTemplate getRestTemplate() {
    return restTemplate;
  }

  @Override
  public String getBaseUri() {
    return baseUri;
  }

  @Override
  public String getClientName() {
    return CLIENT_NAME;
  }
  
  public String sayHello() throws AccessThirdPartyException {
    return exchange("/hello", HttpMethod.POST, MediaType.APPLICATION_JSON, null, null, null, String.class);
  }

}
