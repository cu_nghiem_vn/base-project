package com.truemoney.baseproject.client.exception;

public class InteractiveThirdPartyException extends AccessThirdPartyException{

  private static final long serialVersionUID = 1L;
  private Integer httpCode;
  public InteractiveThirdPartyException(Integer httpCode, String message) {
    super(message);
    this.httpCode = httpCode;
  }
  public Integer getHttpCode() {
    return httpCode;
  }
}
