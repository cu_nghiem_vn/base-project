package com.truemoney.baseproject.client.exception;

public class AccessThirdPartyException extends Exception{

  private static final long serialVersionUID = 1L;
  public AccessThirdPartyException(String message) {
    super(message);
  }
}
