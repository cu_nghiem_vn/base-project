package com.truemoney.baseproject.service;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.truemoney.baseproject.repository.CustomerRespository;
import com.truemoney.baseproject.repository.entity.CustomerEntity;

@Component
public class CustomerService {

  @Autowired
  private CustomerRespository customerRespository;
  
  public void saveCustomer(CustomerEntity entity) {
    customerRespository.save(entity);
  }
  
  public CustomerEntity getById(Long id) {
    Optional<CustomerEntity> entityOp = customerRespository.findById(id);
    if(entityOp.isPresent())
      return entityOp.get();
    return null;
  }
  
  public void updateCustomer(CustomerEntity entity) {
    customerRespository.save(entity);
  }
  
}
