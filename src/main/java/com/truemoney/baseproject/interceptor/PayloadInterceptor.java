package com.truemoney.baseproject.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import com.truemoney.baseproject.constant.PayloadConstant;
import com.truemoney.baseproject.controller.model.Payload;

@Component
public class PayloadInterceptor extends HandlerInterceptorAdapter {

  private static Logger logger = LoggerFactory.getLogger(PayloadInterceptor.class);

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {
    Payload payload = new Payload();
    Long userId = ((request.getHeader(PayloadConstant.USER_ID_KEY) == null) ? null : Long.parseLong(request.getHeader(PayloadConstant.USER_ID_KEY)));
    Integer userTypeId = ((request.getHeader(PayloadConstant.USER_TYPE_ID) == null) ? null : Integer.parseInt(request.getHeader(PayloadConstant.USER_TYPE_ID)));
    payload.setAccessToken(request.getHeader(PayloadConstant.ACCESS_TOKEN_KEY));
    payload.setUserId(userId);
    payload.setUserTypeId(userTypeId);
    payload.setClientId(request.getHeader(PayloadConstant.CLIENT_ID));
    payload.setDeviceId(request.getHeader(PayloadConstant.DEVICE_ID));
    payload.setDeviceDescription(request.getHeader(PayloadConstant.DEVICE_DESCRIPTION));
    payload.setDeviceImei(request.getHeader(PayloadConstant.DEVICE_IMEI));
    payload.setDeviceModel(request.getHeader(PayloadConstant.DEVICE_MODEL));
    payload.setDeviceName(request.getHeader(PayloadConstant.DEVICE_NAME));
    payload.setOsName(request.getHeader(PayloadConstant.OS_NAME));
    payload.setOsVersion(request.getHeader(PayloadConstant.OS_VERSION));
    payload.setNetworkName(request.getHeader(PayloadConstant.NETWORK_NAME));
    if (request.getHeader(PayloadConstant.IPV4_ADDRESS) != null && !request.getHeader(PayloadConstant.IPV4_ADDRESS).isEmpty()) {
      payload.setIpAddress(request.getHeader(PayloadConstant.IPV4_ADDRESS));
    } else if (request.getHeader(PayloadConstant.IPV6_ADDRESS) != null && !request.getHeader(PayloadConstant.IPV6_ADDRESS).isEmpty()) {
      payload.setIpAddress(request.getHeader(PayloadConstant.IPV6_ADDRESS));
    }
    request.setAttribute(PayloadConstant.PAYLOAD_KEY, payload);
    logger.info(
        "Payload from user id [{}], user type id [{}], client id [{}], device id [{}], device description [{}], "
            + "[{}] device imei, [{}] device name, [{}] device model, [{}] os name, [{}] os version, [{}] network name, [{}] ip address",
        payload.getUserId(), payload.getUserTypeId(), payload.getClientId(), payload.getDeviceId(),
        payload.getDeviceDescription(), payload.getDeviceImei(), payload.getDeviceName(),
        payload.getDeviceModel(), payload.getOsName(), payload.getOsVersion(),
        payload.getNetworkName(), payload.getIpAddress());
    return true;
  }
}
