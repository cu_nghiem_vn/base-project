package com.truemoney.baseproject.constant;

public class PayloadConstant {

    public static final String PAYLOAD_KEY = "payload";
    public static final String USER_ID_KEY = "user_id";
    public static final String USER_TYPE_ID = "user_type_id";
    public static final String USER_TYPE_NAME = "user_type_name";
    public static final String ACCESS_TOKEN_KEY = "access_token";
    public static final String CLIENT_ID = "client_id";
    public static final String DEVICE_ID = "device_id";
    public static final String IPV4_ADDRESS = "device_ipv4";
    public static final String IPV6_ADDRESS = "device_ipv6";
    public static final String DEVICE_DESCRIPTION = "device_description";
    public static final String DEVICE_IMEI = "device_imei";
    public static final String DEVICE_NAME = "device_name";
    public static final String DEVICE_MODEL = "device_model";
    public static final String OS_NAME = "device_os_name";
    public static final String OS_VERSION = "device_os_ver";
    public static final String NETWORK_NAME = "network_name";
}
