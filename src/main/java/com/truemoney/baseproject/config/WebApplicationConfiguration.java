package com.truemoney.baseproject.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import com.truemoney.baseproject.interceptor.PayloadInterceptor;

@Configuration
@EnableWebMvc
public class WebApplicationConfiguration implements WebMvcConfigurer {

  @Autowired
  private PayloadInterceptor payloadInterceptor;

  @Override
  public void addInterceptors(final InterceptorRegistry registry) {
    registry.addInterceptor(payloadInterceptor).addPathPatterns("/**");
  }
}
