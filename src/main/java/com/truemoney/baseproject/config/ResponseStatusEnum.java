package com.truemoney.baseproject.config;

public enum ResponseStatusEnum {
  SUCCESS("PX00000", "Success"),
  REQUIRED_NAME_ERROR("PX00001", "Require name"),
  REQUIRED_AGE_ERROR("PX00002", "Require age"),
  REQUIRED_ADDRESS_ERROR("PX00003", "Require address"),
  DUPLICATED_MSISDN_ERROR("PX00020", "Duplicated Msisdn"),
  PROVIDER_NOT_AVAILABLE_ERROR("PX000100", "Provider not available"),
  PROVIDER_ERROR("PX000101", "Provider error"),
  GENERAL_ERROR("PX99999", "General error");
  
  private String code;
  private String message;

  private ResponseStatusEnum(String code, String message) {
      this.code = code;
      this.message = message;
  }

  public String code() {
      return code;
  }

  public String message() {
      return message;
  }
}
