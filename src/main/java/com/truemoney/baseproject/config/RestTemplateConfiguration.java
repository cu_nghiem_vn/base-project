package com.truemoney.baseproject.config;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import com.truemoney.baseproject.util.RestTemplateFactory;

@Configuration
public class RestTemplateConfiguration {
  
  @Value("${application.rest.client.readtimeout}")
  private int readTimeout;

  @Value("${application.rest.client.maxperroute}")
  private int maxPerRoute;

  @Value("${application.rest.client.maxtotalconnection}")
  private int maxTotalConnection;

  public int getConnectionTimeout() {
      return readTimeout * 5 / 100;
  }

  @Bean
  public RestTemplate restTemplate() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
      return RestTemplateFactory.build(getConnectionTimeout(), readTimeout, maxPerRoute, maxTotalConnection);
  }
}
