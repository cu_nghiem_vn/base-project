package com.truemoney.baseproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.truemoney.baseproject.repository.entity.CustomerEntity;

@Repository
public interface CustomerRespository extends JpaRepository<CustomerEntity, Long>{

}
