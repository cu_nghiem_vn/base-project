package com.truemoney.baseproject.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.truemoney.baseproject.config.ResponseStatusEnum;
import com.truemoney.baseproject.controller.model.Customer;
import com.truemoney.baseproject.controller.response.CustomerResponse;
import com.truemoney.baseproject.controller.response.GeneralResponse;
import com.truemoney.baseproject.controller.response.UpdateCustomerResponse;
import com.truemoney.baseproject.controller.response.util.ResponseFactory;
import com.truemoney.baseproject.endpoint.CustomerEnpoint;
import com.truemoney.baseproject.exception.InvalidRequestException;
import com.truemoney.baseproject.exception.ThirdPartyException;

@RestController
@RequestMapping(value = "/v${api.version}")
public class CustomerController {
  
  private static Logger logger = LoggerFactory.getLogger(CustomerController.class);
  
  @Autowired
  private CustomerEnpoint customerEnpoint;
  
  @PostMapping(value = "/customers", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<GeneralResponse<Object>> createCustomer(@RequestBody Customer customer){
    logger.info("===== Start createCustomer =====");
    try {
      Long id = customerEnpoint.saveCustomer(customer);
      CustomerResponse data = new CustomerResponse();
      data.setId(id);
      logger.info("===== End createCustomer =====");
      return ResponseFactory.success(data);
    } catch (InvalidRequestException e) {
      logger.error("Error occurr when create customer [{}]", e.getMessage());
      return ResponseFactory.error(HttpStatus.BAD_REQUEST, e.getErrorCode(), e.getMessage());
    }catch (Exception e) {
      logger.error("Error occurr when create customer [{}]", e.getMessage());
      return ResponseFactory.error(HttpStatus.INTERNAL_SERVER_ERROR, ResponseStatusEnum.GENERAL_ERROR);
    }
  }
  
  @GetMapping(value = "/customers/{id}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<GeneralResponse<Object>> getById(@PathVariable(value = "id") Long customerId){
    logger.info("===== Start getCustomer =====");
    try {
      Customer customer = customerEnpoint.getById(customerId);
      logger.info("===== End getCustomer =====");
      return ResponseFactory.success(customer);
    }catch (Exception e) {
      logger.error("Error occurr when getCustomer [{}]", e.getMessage());
      return ResponseFactory.error(HttpStatus.INTERNAL_SERVER_ERROR, ResponseStatusEnum.GENERAL_ERROR);
    }
  }
  
  @PutMapping(value = "/customers", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<GeneralResponse<Object>> updateCustomer(@RequestBody Customer customer){
    logger.info("===== Start updateCustomer =====");
    try {
      String msg = customerEnpoint.updateCustomer(customer);
      UpdateCustomerResponse data = new UpdateCustomerResponse();
      data.setMessage(msg);
      logger.info("===== End updateCustomer =====");
      return ResponseFactory.success(data);
    } catch (InvalidRequestException | ThirdPartyException e) {
      logger.error("Error occurr when updateCustomer [{}]", e.getMessage());
      return ResponseFactory.error(HttpStatus.BAD_REQUEST, e.getErrorCode(), e.getMessage());
    }catch (Exception e) {
      logger.error("Error occurr when updateCustomer [{}]", e.getMessage());
      return ResponseFactory.error(HttpStatus.INTERNAL_SERVER_ERROR, ResponseStatusEnum.GENERAL_ERROR);
    }
  }
  
  
}
