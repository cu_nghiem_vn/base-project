package com.truemoney.baseproject.controller.response;

public class UpdateCustomerResponse {
  private String message;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
  
  
}
