package com.truemoney.baseproject.controller.response;

public class CustomerResponse {
  private Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
  
}
