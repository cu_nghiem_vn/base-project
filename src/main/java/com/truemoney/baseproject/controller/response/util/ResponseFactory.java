package com.truemoney.baseproject.controller.response.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.truemoney.baseproject.config.ResponseStatusEnum;
import com.truemoney.baseproject.controller.response.GeneralResponse;

public class ResponseFactory {

  private ResponseFactory() {}

  public static ResponseEntity<GeneralResponse<Object>> success() {
    GeneralResponse<Object> responseObject = new GeneralResponse<>();
    responseObject.setCode(ResponseStatusEnum.SUCCESS.code());
    responseObject.setMsg(ResponseStatusEnum.SUCCESS.message());
    return ResponseEntity.ok(responseObject);
  }

  public static <T> ResponseEntity<GeneralResponse<T>> success(T data) {
    GeneralResponse<T> responseObject = new GeneralResponse<>();
    responseObject.setCode(ResponseStatusEnum.SUCCESS.code());
    responseObject.setMsg(ResponseStatusEnum.SUCCESS.message());
    responseObject.setData(data);
    return new ResponseEntity<GeneralResponse<T>>(responseObject, HttpStatus.OK);
  }

  public static ResponseEntity<GeneralResponse<Object>> error(HttpStatus httpStatus, ResponseStatusEnum status) {
    GeneralResponse<Object> responseObject = new GeneralResponse<>();
    responseObject.setCode(status.code());
    responseObject.setMsg(status.message());
    return new ResponseEntity<GeneralResponse<Object>>(responseObject, httpStatus);
  }

  public static ResponseEntity<GeneralResponse<Object>> error(HttpStatus httpStatus, String code, String message) {
    GeneralResponse<Object> responseObject = new GeneralResponse<>();
    responseObject.setCode(code);
    responseObject.setMsg(message);
    return new ResponseEntity<GeneralResponse<Object>>(responseObject, httpStatus);
  }
}
