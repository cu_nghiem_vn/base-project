package com.truemoney.baseproject.exception;

public abstract class BaseException extends RuntimeException {
  private static final long serialVersionUID = -4814001996644562280L;

  public BaseException(String message) {
    super(message);
  }

  public abstract String getErrorCode();
  
}
