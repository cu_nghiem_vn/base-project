package com.truemoney.baseproject.exception;

public class ThirdPartyException extends BaseException {
  private String errorCode;
  
  public ThirdPartyException(String errorCode, String message) {
    super(message);
    this.errorCode = errorCode;
  }

  @Override
  public String getErrorCode() {
    return errorCode;
  }

}
