package com.truemoney.baseproject.exception;

public class InvalidRequestException extends BaseException {
  private static final long serialVersionUID = 1L;
  private String errorCode;
  
  public InvalidRequestException(String errorCode, String errorMessage) {
    super(errorMessage);
    this.errorCode = errorCode;
  }

  @Override
  public String getErrorCode() {
    return errorCode;
  }

}
