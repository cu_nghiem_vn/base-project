package com.truemoney.baseproject.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonObject;

public class MaskingSensitiveDataUtil {
  private final static Logger logger = LoggerFactory.getLogger(MaskingSensitiveDataUtil.class);

  private MaskingSensitiveDataUtil() {}

  public static final String BASE_UUID_REGEX = "\\p{XDigit}{8}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{12}";

  protected static List<String> sensitiveFields = new ArrayList<>();
  
  static {
    sensitiveFields.add("password");
    sensitiveFields.add("pin");
    sensitiveFields.add("access_token");
    sensitiveFields.add("otp");
    sensitiveFields.add("security_code");
    sensitiveFields.add("refresh_token");
    sensitiveFields.add("verify_token");
    sensitiveFields.add("pan");
    sensitiveFields.add("cvv");
    sensitiveFields.add("token");
    sensitiveFields.add("bank_token");
    sensitiveFields.add("bank_account_number");
    sensitiveFields.add("card_token");
    sensitiveFields.add("code_number");
    sensitiveFields.add("3d_secure_code");
  }


  public static <T> T maskSensitiveObject(Object obj, Class<T> clazz) {
    T sensitiveObject = null;
    try {
      if (obj == null) {
        return null;
      }
      JSONObject jsonObject = new JSONObject(Util.objectToJson(obj));
      Iterator<?> keys = jsonObject.keys();
      while (keys.hasNext()) {
        String item = (String) keys.next();
        if (jsonObject.get(item) instanceof JSONObject) {
          sensitiveObject = maskSensitiveObject(jsonObject.get(item), clazz);
          jsonObject.remove(item);
          jsonObject.put(item, sensitiveObject);
        } else {
          if (sensitiveFields.contains(item)) {
            String content = jsonObject.getString(item);
            if (StringUtils.isNotBlank(content)) {
              jsonObject.remove(item);
              jsonObject.put(item, maskString(content, true));
            }
          }
        }
      }

      if (jsonObject != null) {
        sensitiveObject = Util.jsonToObject(jsonObject.toString(), clazz);
      }

      if (sensitiveObject == null) {
        sensitiveObject = (T) obj;
      }

    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }

    return sensitiveObject;
  }

  public static String maskSensitiveInfo(String json, boolean isMaskingAll) {
    String sensitiveString = null;
    try {
      if (StringUtils.isBlank(json) || "null".equals(json)) {
        return null;
      }
      JSONObject jsonObject = new JSONObject(json);
      Iterator<?> keys = jsonObject.keys();
      while (keys.hasNext()) {
        String key = (String) keys.next();
        Object obj = jsonObject.get(key);
        if (obj instanceof JSONObject) {
          JSONObject jsObj = (JSONObject) obj;
          sensitiveString = maskSensitiveInfo(jsObj.toString(), isMaskingAll);
          jsonObject.remove(key);
          jsonObject.put(key, sensitiveString);
        } else if (obj instanceof JSONArray) {
          if (sensitiveFields.contains(key)) {
            JSONArray jsonArray = (JSONArray) obj;
            for (int i = 0; i < jsonArray.length(); i++) {
              Object elem = jsonArray.get(i);
              if (elem instanceof JSONObject) {
                sensitiveString = maskSensitiveInfo(((JsonObject) elem).toString(), isMaskingAll);
                jsonArray.remove(i);
                jsonArray.put(i, sensitiveString);
              } else {
                String content = elem.toString();
                if (StringUtils.isNotBlank(content)) {
                  jsonArray.remove(i);
                  jsonArray.put(i, maskString(content, isMaskingAll));
                }
              }
            }
          }
        } else {
          if (sensitiveFields.contains(key)) {
            String content = obj.toString();
            if (StringUtils.isNotBlank(content)) {
              jsonObject.remove(key);
              jsonObject.put(key, maskString(content, isMaskingAll));
            }
          }
        }

      }

      if (jsonObject != null) {
        sensitiveString = jsonObject.toString();
      }

    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }

    return sensitiveString;
  }

  public static JSONObject maskSensitiveObject(Object obj) {
    return maskSensitiveObject(Util.objectToJson(obj));
  }

  public static <T> T maskSensitiveObject(String json, Class<T> clazz) {
    T sensitiveObject = null;
    try {
      if (StringUtils.isBlank(json)) {
        return null;
      }
      String maskString = maskSensitiveInfo(json, false);
      if (StringUtils.isNotBlank(maskString)) {
        sensitiveObject = Util.jsonToObject(maskString, clazz);
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }

    return sensitiveObject;
  }

  public static String maskSensitiveUrl(final String url) {
    String maskUrl = null;
    try {
      if (StringUtils.isBlank(url)) {
        return null;
      }
      maskUrl = url.replaceAll(BASE_UUID_REGEX, "***");
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }

    return maskUrl;
  }


  public static String maskString(String str, Boolean isMashAll) {
    if (StringUtils.isBlank(str)) {
      return null;
    }
    if (isMashAll) {
      return str = str.replaceAll(".", "*");
    }
    Integer numberOfDisplayDigit = 3;
    Integer length = str.length();
    String displayDigit = "";
    if (length < numberOfDisplayDigit) {
      return str;
    }
    displayDigit = str.subSequence(length - numberOfDisplayDigit, length).toString();
    str = str.replaceAll(".", "*").substring(0, length - numberOfDisplayDigit);
    return str + displayDigit;

  }
}
