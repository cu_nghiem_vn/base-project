package com.truemoney.baseproject.util;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.SSLContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustAllStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

public class RestTemplateFactory {

  public static final Integer DEFAULT_MAX_PER_ROUTE = 40;
  public static final Integer DEFAULT_MAX_TOTAL_CONNECTION = 100;

  private RestTemplateFactory() {}

  public static RestTemplate build(int connectionTimeout, int readTimeout) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
    return new RestTemplate(getClientHttpRequestFactory(connectionTimeout, readTimeout, DEFAULT_MAX_PER_ROUTE, DEFAULT_MAX_TOTAL_CONNECTION));
  }

  public static RestTemplate build(int connectionTimeout, int readTimeout, int maxPerRoute, int maxTotalConnection) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
    return new RestTemplate(getClientHttpRequestFactory(connectionTimeout, readTimeout, maxPerRoute, maxTotalConnection));
  }

  private static ClientHttpRequestFactory getClientHttpRequestFactory(int connectionTimeout, int readTimeout, int maxPerRoute, int maxTotalConnection) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
    HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(getHttpClient(maxPerRoute, maxTotalConnection));
    factory.setConnectTimeout(connectionTimeout);
    factory.setReadTimeout(readTimeout);
    return factory;
  }

  private static CloseableHttpClient getHttpClient(int maxPerRoute, int maxTotalConnection) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
    Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create().register("HTTPS", getSSLConnectionSocketFactory()).register("HTTP", PlainConnectionSocketFactory.getSocketFactory()).build();
    PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(registry);
    connectionManager.setDefaultMaxPerRoute(maxPerRoute);
    connectionManager.setMaxTotal(maxTotalConnection);
    SSLConnectionSocketFactory connSocketFactory = getSSLConnectionSocketFactory();
    return HttpClients.custom().setConnectionManager(connectionManager).setSSLSocketFactory(connSocketFactory).build();
  }

  private static SSLConnectionSocketFactory getSSLConnectionSocketFactory()
      throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
    TrustStrategy trustStrategy = new TrustAllStrategy();
    SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, trustStrategy).build();
    String[] supportedProtocols = getSupportedProtocols();
    return new SSLConnectionSocketFactory(sslContext, supportedProtocols, null, new NoopHostnameVerifier());
  }

  private static String[] getSupportedProtocols() {
    return new String[] {"TLSv1", "TLSv1.1", "TLSv1.2"};
  }
}
