package com.truemoney.baseproject.util;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class Util {

  private static Logger logger = LoggerFactory.getLogger(Util.class);
  
  private Util() {}

  public static String objectToJson(Object obj) {
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
      return objectMapper.writeValueAsString(obj);
    } catch (JsonProcessingException e) {
      logger.error("Error when writing object to json string [{}]", e.getMessage());
    }
    return null;
  }

  public static <T> T jsonToObject(String json, Class<T> clazz) {
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.readValue(json, clazz);
    } catch (IOException e) {
      logger.error("Error when reading json string to object [{}]", e.getMessage());
    }
    return null;
  }

}
