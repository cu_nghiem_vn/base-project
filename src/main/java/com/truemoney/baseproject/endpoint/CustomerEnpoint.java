package com.truemoney.baseproject.endpoint;

import java.util.Optional;
import javax.transaction.Transactional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.truemoney.baseproject.config.ResponseStatusEnum;
import com.truemoney.baseproject.controller.model.Customer;
import com.truemoney.baseproject.endpoint.thirdparty.ExampleEndpoint;
import com.truemoney.baseproject.exception.InvalidRequestException;
import com.truemoney.baseproject.repository.entity.CustomerEntity;
import com.truemoney.baseproject.service.CustomerService;

@Service
public class CustomerEnpoint {

  @Autowired
  private CustomerService customerService;
  
  @Autowired
  private ExampleEndpoint exampleEndpoint;
  
  public Long saveCustomer(Customer customer) {
    if(StringUtils.isBlank(customer.getName()))
      throw new InvalidRequestException(ResponseStatusEnum.REQUIRED_NAME_ERROR.code(), ResponseStatusEnum.REQUIRED_NAME_ERROR.message());
    if(StringUtils.isBlank(customer.getAddress()))
      throw new InvalidRequestException(ResponseStatusEnum.REQUIRED_ADDRESS_ERROR.code(), ResponseStatusEnum.REQUIRED_ADDRESS_ERROR.message());
    if(customer.getAge() == null)
      throw new InvalidRequestException(ResponseStatusEnum.REQUIRED_AGE_ERROR.code(), ResponseStatusEnum.REQUIRED_AGE_ERROR.message());
    CustomerEntity customerEntity = new CustomerEntity();
    customerEntity.setName(customer.getName());
    customerEntity.setAge(customer.getAge());
    customerEntity.setAddress(customer.getAddress());
    customerService.saveCustomer(customerEntity);
    return customerEntity.getId();
  }
  
  public Customer getById(Long id) {
    Customer customer = null;
    CustomerEntity customerEntity = customerService.getById(id);
    if(customerEntity != null) {
      customer = new Customer();
      customer.setId(customerEntity.getId());
      customer.setName(customerEntity.getName());
      customer.setAge(customerEntity.getAge());
      customer.setAddress(customerEntity.getAddress());
    }
    return customer;
  }
  
  @Transactional
  public String updateCustomer(Customer customer) {
    if(StringUtils.isBlank(customer.getName()))
      throw new InvalidRequestException(ResponseStatusEnum.REQUIRED_NAME_ERROR.code(), ResponseStatusEnum.REQUIRED_NAME_ERROR.message());
    if(StringUtils.isBlank(customer.getAddress()))
      throw new InvalidRequestException(ResponseStatusEnum.REQUIRED_ADDRESS_ERROR.code(), ResponseStatusEnum.REQUIRED_ADDRESS_ERROR.message());
    if(customer.getAge() == null)
      throw new InvalidRequestException(ResponseStatusEnum.REQUIRED_AGE_ERROR.code(), ResponseStatusEnum.REQUIRED_AGE_ERROR.message());
    CustomerEntity customerEntity = new CustomerEntity();
    customerEntity.setName(customer.getName());
    customerEntity.setAge(customer.getAge());
    customerEntity.setAddress(customer.getAddress());
    customerService.updateCustomer(customerEntity);
    return exampleEndpoint.getHello();
  }
  
}
