package com.truemoney.baseproject.endpoint.thirdparty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.truemoney.baseproject.client.ExampleClient;
import com.truemoney.baseproject.client.exception.AccessThirdPartyException;
import com.truemoney.baseproject.client.exception.InteractiveThirdPartyException;
import com.truemoney.baseproject.config.ResponseStatusEnum;
import com.truemoney.baseproject.exception.ThirdPartyException;

@Service
public class ExampleEndpoint {
  
  @Autowired
  private ExampleClient exampleClient;
  
  public String getHello() {
    try {
      return exampleClient.sayHello();
    } catch (InteractiveThirdPartyException e) {
      throw new ThirdPartyException(ResponseStatusEnum.PROVIDER_ERROR.code(), ResponseStatusEnum.PROVIDER_ERROR.message());
    }catch (AccessThirdPartyException e) {
      throw new ThirdPartyException(ResponseStatusEnum.PROVIDER_NOT_AVAILABLE_ERROR.code(), ResponseStatusEnum.PROVIDER_NOT_AVAILABLE_ERROR.message());
    }
  }
  
}
